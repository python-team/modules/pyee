pyee (11.1.0-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 11.1.0 (Closes: #1058298)
  * remove old python3-mock build dependency
  * use new dh-sequence-python3

 -- Alexandre Detiste <tchet@debian.org>  Wed, 10 Jan 2024 18:10:21 +0100

pyee (9.0.4-1) unstable; urgency=medium

  [ upstream ]
  * new release

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 21 Feb 2022 22:04:37 +0100

pyee (9.0.3-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Remove MIA uploader Ethan Ward <ethan.ward@mycroft.ai>. Closes: #971865
  * Bump debhelper from old 12 to 13.

  [ Jonas Smedegaard ]
  * add myself as uploader
    closes: bug#971865, thanks for past contributions to Ethan Ward
  * update watch file:
    + use format 4
    + track Github source
    + add filenamemangle
    + set dversionmangle=auto
    + add usage comment
  * update copyright info:
    + list Github as source
    + add Upstream-Contact
    + use SPDX license shortname
    + update coverage;
      license my contributions as GPL-3+
  * add git-buildpackage config:
    + sign tags
    + use pristine-tar
    + filter-out any .git* file
    + use DEP-14 branch naming scheme
    + add usage comment
  * annotate test-only build-dependencies
  * add source helper script copyright-check
  * declare compliance with Debian Policy 4.6.0
  * set Rules-Requires-Root: no
  * drop obsolete patches
  * (build-)depend on python3-typing-extensions

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 01 Feb 2022 22:51:48 +0100

pyee (7.0.2-1) unstable; urgency=medium

  * Team Upload.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Sophie Brun ]
  * New upstream version 7.0.2 (Closes: #956914)
  * Update debian/copyright
  * Refresh patch
  * Disable trio tests (pytest-trio is not in Debian)
  * Add missing build-dep: python3-pytest-asyncio
  * Add autopkgtest-pkg-python
  * Bump Standards-Version to 4.5.0

 -- Raphaël Hertzog <raphael@offensive-security.com>  Wed, 08 Jul 2020 16:19:39 +0200

pyee (3.0.3-2) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.

  [ Andrey Rahmatullin ]
  * Drop Python 2 support.

 -- Andrey Rahmatullin <wrar@debian.org>  Sun, 11 Aug 2019 01:00:48 +0500

pyee (3.0.3-1) unstable; urgency=medium

  * Initial Debian packaging (closes: #869099)

 -- Ethan Ward <ethan.ward@mycroft.ai>  Mon, 17 Jul 2017 10:25:02 -0500
